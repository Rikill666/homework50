class Product {
    constructor(title, callories) {
        this.title = title;
        this.callories = callories;
    }
}

class Dish {
    constructor(title) {
        this.title = title;
        this.products = [];
        this.allCallories = 0;
    }

    addProduct(product, weight) {
        let prod = {
            product,
            weight,
            callories: product.callories / 100 * weight
        };
        this.allCallories += prod.callories;
        this.products.push(prod);
    }

    getDishCallories() {
        let result = "На одну порцию:\n";
        this.products.forEach((product) => {
            result +=`* ${product.product.title}, ${product.weight}гр., ${product.callories}ккал\n`;
            }
        );
        return result;
    }
}
class CaloriesCalculator {
    constructor() {
        this.allCallories = 0;
        this.dishes = [];
    }
    addDish(dish) {
        this.allCallories += dish.allCallories;
        let index = this.dishes.findIndex(obj => obj.dish.title === dish.title);
        if(index === -1){
            let dishesCount = {
                dish,
                count: 1
            };
            this.dishes.push(dishesCount);
        }
        else{
            this.dishes[index].count++;
        }

    }
    getTotalCalories() {
        return `Общая каллорийность: ${this.allCallories}ккал\n`;
    }
    getAllDishesInfo(){
        let result = "";
        this.dishes.forEach((dish) => {
                result += `${dish.dish.title} ${dish.count} порц., ${dish.count*dish.dish.allCallories}ккал. \n`;
                result += `${dish.dish.getDishCallories()} \n`;
            }
        );
        return result;
    }
}
const meat = new Product('Филе говядина', 158);
const rice = new Product('Рис', 130);
const onion = new Product('Лук', 40);
const carrot = new Product('Морковь', 41);

const plov = new Dish('Плов');
plov.addProduct(meat, 100);
plov.addProduct(rice, 150);
plov.addProduct(onion, 25);
plov.addProduct(carrot, 25);

const plov2 = new Dish('Неплов');
plov2.addProduct(meat, 75);
plov2.addProduct(rice, 200);
plov2.addProduct(onion, 20);
plov2.addProduct(carrot, 25);

const plov3 = new Dish('Ваще неплов');
plov3.addProduct(meat, 755);
plov3.addProduct(rice, 20);
plov3.addProduct(onion, 210);
plov3.addProduct(carrot, 96);

const calculator = new CaloriesCalculator();
calculator.addDish(plov);
calculator.addDish(plov);
calculator.addDish(plov2);
calculator.addDish(plov3);
const calories = calculator.getTotalCalories();
console.log(calories); // должно вывести 373.25

const totals = calculator.getAllDishesInfo();
console.log(totals);